// a
function primeArrya(num) {
  function nextPrime(value) {
    if (value > 2) {
      do {
        i = 3;
        value += 2;
        q = Math.floor(Math.sqrt(value));
        while (i <= q && value % i) {
          i += 2;
        }
      } while (i <= q);
      return value;
    }
    return value === 2 ? 3 : 2;
  }
  let value = 0,
    result = [];
  for (let i = 0; i < num; i++) {
    value = nextPrime(value);
    result.push(value);
  }
  return result;
}

console.log(primeArrya(15));

// b
let a = "У Пети было 10 яблок, 2.5 он отдал Маше, 3.5 Васе и 4 оставил себе.";
function _find(str) {
  return str.match(/\d+(?:\.\d+)?/g).map(Number);
}
console.log(_find(a));

// c
const str = "assdssddffffrrreeeweggggg";


function _repleaser(srt){
    let c = str.match(/([\w]{1})\1+/g);
let num = 0;
for(let i = 0; i < c.length; i++ ){
     num = c[i].length;}
return num;
}
console.log(_repleaser(str));

a = str.replace(/([\w]{1})\1+/g, _repleaser);
console.log(a);


// d
const get = (obj, path) =>
  path
    .replace(/\[|\]\.?/g, ".")
    .split(".")
    .filter((s) => s)
    .reduce((acc, val) => acc && acc[val], obj);

const exampleArr = [
  { a: { b: [{ c: 4 }, { c: 5 }] } },
  { a: { b: [{ c: 6 }, { c: 7 }] } },
];
const exampleObj = { a: { b: { c: 5 } } };
// get(exampleArr, '0.a.b.1.c') // 5
// get(exampleObj, 'a.b') // {c:5}

console.log(get(exampleArr, "0.a.b.1.c"));

console.log(get(exampleObj, "a.b"));

// e
function makeRand() {
  let usedNumbers = [];

  function f() {
    if (usedNumbers.length === 100) {
      return;
    }

    var num = Math.floor(Math.random() * 100) + 1;

    for (let i = 0; i < usedNumbers.length; i++) {
      if (num === usedNumbers[i]) return f();
    }

    usedNumbers.push(num);

    return num;
  }

  return f;
}

console.log(makeRand());


// f
